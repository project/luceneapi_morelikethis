<?php

/**
 * @file
 * Admin settings for Search Lucene MoreLikeThis.
 * 
 * @ingroup luceneapi_morelikethis
 */

/**
 * Submit handler, clears cached priority queues.
 *
 * @param $form
 *   A FAPI array modeling the submitted form.
 * @param &$form_state
 *   An array containing the current state of the form.
 * @return
 *   NULL
 */
function luceneapi_morelikethis_admin_settings_submit($form, &$form_state) {
  cache_clear_all('luceneapi_morelikethis:', LUCENEAPI_CACHE_TABLE, TRUE);
}

/**
 * Administrative settings.
 *
 * @return
 *   An array of form elements.
 * @ingroup forms
 */
function luceneapi_morelikethis_admin_settings() {
  $form = array();

  $form['description'] = array(
    '#value' => t(
      'Search Lucene MoreLikeThis adds content recommendations via the <em>Search Lucene MoreLikeThis</em> <a href="@block-page">block</a>. By comparing the text in the node to the Lucene index, Search Lucene MoreLikeThis is able to determine the important words and execute a weighted search query to find similar pieces of content.',
      array('@block-page' => url('admin/build/block/list'))
    ),
    '#weight' => -60,
  );

  // Boost terms in query based on score.
  $form['search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search settings'),
    '#collapsible' => TRUE,
    '#weight' => -60,
  );

  // common options for items that have a minimum
  $options = array(0 => t('No minimum'));

  $form['search']['luceneapi_morelikethis:num_displayed'] = array(
    '#type' => 'select',
    '#title' => t('Number of Recommendations'),
    '#default_value' => variable_get('luceneapi_morelikethis:num_displayed', 5),
    '#options' => drupal_map_assoc(range(1, 10)),
    '#description' => t('The number of recommendations displayed in the block.'),
  );

  $form['search']['luceneapi_morelikethis:empty_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Message for no recommendations'),
    '#default_value' => variable_get(
      'luceneapi_morelikethis:empty_message',
      t('No recommendations were found.')
    ),
    '#description' => t('The message displayed in the block when no recommendations are returned. Empty the text in this field to hide the block if no recommendations are returned.'),
  );

  $form['filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Term filters'),
    '#collapsible' => TRUE,
    '#weight' => -40,
  );

  $form['filter']['luceneapi_morelikethis:min_word_length'] = array(
    '#type' => 'select',
    '#title' => t('Minimum word length'),
    '#default_value' => variable_get('luceneapi_morelikethis:min_word_length', 3),
    '#options' => $options + drupal_map_assoc(range(1, 10)),
    '#description' => t('Ignore words less than this length.'),
  );

  $form['filter']['luceneapi_morelikethis:max_word_length'] = array(
    '#type' => 'select',
    '#title' => t('Maximum word length'),
    '#default_value' => variable_get('luceneapi_morelikethis:max_word_length', 0),
    '#options' => array(0 => t('No limit')) + drupal_map_assoc(range(3, 15)),
    '#description' => t('Ignore words greater than this length.'),
  );

  $form['filter']['luceneapi_morelikethis:min_term_freq'] = array(
    '#type' => 'select',
    '#title' => t('Minimum term frequency'),
    '#default_value' => variable_get('luceneapi_morelikethis:min_term_freq', 2),
    '#options' => $options + drupal_map_assoc(range(1, 20)),
    '#description' => t('Ignore terms with less than this frequency in the source text.'),
  );

  $form['filter']['luceneapi_morelikethis:min_doc_freq'] = array(
    '#type' => 'select',
    '#title' => t('Minimum document frequency'),
    '#default_value' => variable_get('luceneapi_morelikethis:min_doc_freq', 5),
    '#options' => $options + drupal_map_assoc(range(1, 20)),
    '#description' => t('Ignore words which do not occur in at least this many nodes.'),
  );

  $form['filter']['luceneapi_morelikethis:max_query_terms'] = array(
    '#type' => 'select',
    '#title' => t('Maximum terms to query'),
    '#default_value' => variable_get('luceneapi_morelikethis:max_query_terms', 25),
    '#options' => $options + drupal_map_assoc(range(0, 100, 5)),
    '#description' => t('Return a Query with no more than this many terms. Lowering this setting will find fewer recommendations, but it will also improve the performance of the query.'),
  );

  $form['search_fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Lucene fields'),
    '#collapsible' => TRUE,
    '#description' => t('Select the Lucene fields that are queried when finding recommendations.'),
    '#weight' => -30,
  );

  // gets bias fields, formats options
  $fields = array_merge(
    module_invoke_all('luceneapi_node_bias_fields'),
    module_invoke_all('luceneapi_node_bias_tags')
  );
  $options = array();
  foreach ($fields as $field => $info) {
    $options[$field] = $info['title'];
  }

  $default = drupal_map_assoc(array('title'));
  $form['search_fields']['luceneapi_morelikethis:search_fields'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Fields'),
    '#default_value' => variable_get('luceneapi_morelikethis:search_fields', $default),
    '#options' => $options,
  );
  
  // source fields
  $form['node_fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node fields'),
    '#collapsible' => TRUE,
    '#description' => t('Select the parts of the node the source text is extracted from.'),
    '#weight' => -20,
  );
  
  // fields that the source text is build from
  $form['node_fields']['luceneapi_morelikethis:node_fields'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Fields'),
    '#options' => module_invoke_all('luceneapi_morelikethis_fields'),
    '#default_value' => variable_get('luceneapi_morelikethis:node_fields', $default),
  );

  // content type settings
  $form['type'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content types'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => -10,
  );

  // content types excluded from recommendations
  $form['type']['luceneapi_morelikethis:excluded_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Types excluded from recommendations'),
    '#options' => array_map('check_plain', node_get_types('names')),
    '#default_value' => variable_get('luceneapi_morelikethis:excluded_types', array()),
    '#description' => t('Adding content types to this list will exclude nodes of the selected types from the recommendations.'),
  );

  return system_settings_form($form);
}
