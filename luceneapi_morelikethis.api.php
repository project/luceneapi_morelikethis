<?php

/**
 * @file
 * Example file that defines the Search Lucene MoreLikeThis hooks.
 *
 * @ingroup luceneapi_morelikethis
 */

/**
 * Allows modules to add to the text that recommendation queries are constructed
 * from.
 * 
 * @param $node
 *   An object containing the node being viewed.
 * @return
 *   A string containing the text to add.
 */
function hook_luceneapi_morelikethis_text_add($node) {
} 

/**
 * Allows modules to define "fields" that will be extracted from the node and
 * used as the source text.  In this case, fields can be items such as the node
 * body, node title, and taxonomy terms in addition to CCK fields.
 * 
 * @return
 *   An associative array keyed by machine-readable field name to the display
 *   name of the field.
 */
function hook_luceneapi_morelikethis_fields() {
}
